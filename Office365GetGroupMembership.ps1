#*=============================================================================
#* Script Name: GetOffice365GroupMembership
#* Created: 02-08-2017
#* Revised: 02-20-2017
#* Author: Matthew Dudlo
#* Purpose: Retrieve a list of users and their associated security group
#*
#*=============================================================================
#*=============================================================================
#* PARAMETER DECLARATION
#*=============================================================================
param(

)
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#*1.1    Added Structure
#*1.2    Added Output Sorting
#*=============================================================================
#* IMPORT LIBRARIES
#*=============================================================================

#*=============================================================================
#* PARAMETERS
#*=============================================================================
#*=============================================================================
#* INITIALISE VARIABLES
#*=============================================================================
# Increase buffer width/height to avoid PowerShell from wrapping the text in
# output files
#$pshost = get-host
#$pswindow = $pshost.ui.rawui
#$newsize = $pswindow.buffersize
#$newsize.height = 3000
#$newsize.width = 500
#$pswindow.buffersize = $newsize
$groups = "Group1","Group2","Group3","Group4","Group5" ###Replace with your own group names###
#*=============================================================================
#* EXCEPTION HANDLER
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
 #*=============================================================================
#* Function:
#* Created:
#* Revised:
#* Author:
#* Purpose:
#* =============================================================================
#*=============================================================================
#* END OF FUNCTION LISTINGS
#*=============================================================================
#*=============================================================================
#* SCRIPT BODY
#*=============================================================================



$groupGuid = foreach ($group in $groups) {
                Get-MsolGroup | where-object displayname -Match $group
                }

foreach ($uid in $groupGuid){
#Write-Host $uid.DisplayName
#Start-Sleep -Seconds 3
    Get-MsolGroup -ObjectId $uid.ObjectId.Guid -OutVariable 1 |Out-Null
    Get-MsolGroupMember -GroupObjectId $uid.objectid.guid -OutVariable 2 |Out-Null
    $2 = $2 | Sort-Object -Property emailaddress
    write-host "$($1.displayname) "  -ForegroundColor  Yellow -NoNewline
    $n = 40 - $1.displayname.length #Add Seperator
        while ($n -gt 0){
            Write-Host "-" -NoNewline
            $n = $n - 1
        }
     Write-Host " " 

     $2.emailaddress | Foreach-Object {Write-Host $_ -ForegroundColor green}


    }
