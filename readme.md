# Automation-Scripts


## Office365GetGroupMembership.ps1
- Retrieves a list of users in defined groups

## Office365AddUser.ps1
- Adds a user account
- Assigns licenses
- Adds Title and Department

## Office365GroupCreate.ps1
- Creates Office365 groups
- adds defined users to the new groups

## Office365GetLicenseCount.ps1
- Calculates and outputs the current number of licenses available to be assigned

##  Office365GetLicenseAssignment.ps1
- Displays the user accounts assigned to each Office365 licenses

## script-test.ps1
- Script for static code analysis for Gitlab CI

## Template.ps1
- Template for powershell script structure

# Requirements
## Static Code Analysis
Install Script Analyzer from PSGallery:
````powershell
Install-Module -Name PSScriptAnalyzer
````
