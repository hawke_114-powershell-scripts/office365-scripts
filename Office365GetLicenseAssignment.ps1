﻿#*=============================================================================
#* Script Name: Office365GetLicenseAssignment.ps1
#* Created: 02-23-2017
#* Revised:
#* Author: Matthew Dudlo
#* Purpose: Output Office365 licenses and the users assigned to them
#*
#*=============================================================================
#*=============================================================================
#* PARAMETER DECLARATION
#*=============================================================================
param(

)
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#*1.1    Added Output Sorting
#*=============================================================================
#* IMPORT LIBRARIES
#*=============================================================================

#*=============================================================================
#* PARAMETERS
#*=============================================================================

#*=============================================================================
#* INITIALISE VARIABLES
#*=============================================================================
# Increase buffer width/height to avoid PowerShell from wrapping the text in
# output files
$tenantID = "your TenantID"
#$pshost = get-host
#$pswindow = $pshost.ui.rawui
#$newsize = $pswindow.buffersize
#$newsize.height = 3000
#$newsize.width = 500
#$pswindow.buffersize = $newsize
#*=============================================================================
#* EXCEPTION HANDLER
#*=============================================================================
#*=============================================================================
#* INITIALISE VARIABLES
#*=============================================================================
# Increase buffer width/height to avoid PowerShell from wrapping the text in
# output files
#$pshost = get-host
#$pswindow = $pshost.ui.rawui
#$newsize = $pswindow.buffersize
#$newsize.height = 3000
#$newsize.width = 500
#$pswindow.buffersize = $newsize
#*=============================================================================
#* EXCEPTION HANDLER
#*=============================================================================

#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================

#*=============================================================================
#* Function: get-Office365Business-Premium
#* Created: 02-23-17
#* Revised:
#* Author: Matthew Dudlo
#* Purpose:
#* =============================================================================
function get-Office365Business-Premium{
  Write-Host -ForegroundColor Yellow "Business Premium"
  $n=0
  while ($n -lt $users.count){
    if ($users[$n].licenses.accountskuid -contains "$($tenantID):O365_BUSINESS_PREMIUM"){Write-Host "$($users[$n].userprincipalname)"}
    $n=$n+1
   }
 }
 #*=============================================================================
#* Function: get-Office365Business-Essentials
#* Created: 02-23-17
#* Revised:
#* Author: Matthew Dudlo
#* Purpose:
#* =============================================================================
function get-Office365Business-Essentials{
  Write-Host -ForegroundColor Yellow "Business Essentials"
  $n=0
  while ($n -lt $users.count){
    if ($users[$n].licenses.accountskuid -contains "$($tenantID):O365_BUSINESS_ESSENTIALS"){Write-Host "$($users[$n].userprincipalname)"}
    $n=$n+1
   }
 }
 #*=============================================================================
#* Function: get-Office365Visio
#* Created: 02-23-17
#* Revised:
#* Author: Matthew Dudlo
#* Purpose:
#* =============================================================================
function get-Office365Visio{
  Write-Host -ForegroundColor Yellow "Visio"
  $n=0
  while ($n -lt $users.count){
    if ($users[$n].licenses.accountskuid -contains "$($tenantID):VISIOCLIENT"){Write-Host "$($users[$n].userprincipalname)"}
    $n=$n+1
   }
 }
 #*=============================================================================
#* Function: get-Office365ProjectPro
#* Created: 02-23-17
#* Revised:
#* Author: Matthew Dudlo
#* Purpose:
#* =============================================================================
function get-Office365ProjectPro{
  Write-Host -ForegroundColor Yellow "Project Pro"
  $n=0
  while ($n -lt $users.count){
    if ($users[$n].licenses.accountskuid -contains "$($tenantID):PROJECTCLIENT"){Write-Host "$($users[$n].userprincipalname)"}
    $n=$n+1
   }
 }
 #*=============================================================================
#* Function: get-Office365EnterpriseE3
#* Created: 02-23-17
#* Revised: Matthew Dudlo
#* Author:
#* Purpose:
#* =============================================================================
function get-Office365EnterpriseE3{
  Write-Host -ForegroundColor Yellow "Enterprise E3"
  $n=0
  while ($n -lt $users.count){
    if ($users[$n].licenses.accountskuid -contains "$($tenantID):ENTERPRISEPACK"){Write-Host "$($users[$n].userprincipalname)"}
    $n=$n+1
   }
 }
 #*=============================================================================
#* Function: get-Office365PSTN
#* Created: 02-23-17
#* Revised: 
#* Author: Matthew Dudlo
#* Purpose:
#* =============================================================================
#* =============================================================================
function get-Office365PSTN{
  Write-Host -ForegroundColor Yellow "Skype For Business PSTN Conferencing"
  $n=0
  while ($n -lt $users.count){
    if ($users[$n].licenses.accountskuid -contains "$($tenantID):MCOMEETADV"){Write-Host "$($users[$n].userprincipalname)"}
    $n=$n+1
   }
 }
#*=============================================================================
#* Function: get-missing-EMS
#* Created: 02-23-17
#* Revised: Matthew Dudlo
#* Author:
#* Purpose:
#* =============================================================================


 function get-missing-EMS{
  Write-Host -ForegroundColor Yellow "Accounts Without MDM Licenses"
  $n=0
  while ($n -lt $users.count){
    if ($users[$n].licenses.accountskuid -notcontains "$($tenantID):EMS"){Write-Host "$($users[$n].userprincipalname)"}
    $n=$n+1
   }
 }
 #*=============================================================================
#* Function: get-overlap
#* Created: 02-23-17
#* Revised:
#* Author: Matthew Dudlo
#* Purpose:
#* =============================================================================

 function get-overlap{
  Write-Host -ForegroundColor Yellow "Overlapping Office Licenses"
  $n=0
  while ($n -lt $users.count){
    if ($users[$n].licenses.accountskuid -contains "$($tenantID):O365_BUSINESS_PREMIUM" -and $users[$n].licenses.accountskuid -contains "$($tenantID):O365_BUSINESS_ESSENTIALS"){Write-Host "$($users[$n].userprincipalname)"}
    $n=$n+1
   }
 }
#*=============================================================================
#* END OF FUNCTION LISTINGS
#*=============================================================================
#*=============================================================================
#* SCRIPT BODY
#*=============================================================================
$users = get-msoluser | sort-object -Property UserPrincipalName
get-Office365Business-Premium
get-Office365Business-Essentials
get-Office365EnterpriseE3
get-Office365Visio
get-Office365ProjectPro
get-Office365PSTN
get-missing-EMS
get-overlap
