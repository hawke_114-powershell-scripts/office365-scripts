﻿$groups = "Group1","Group2","Group3","Group5-Admin","Group5-Projects","Group6","Rubix-Admin","Rubix-Projects","Security-Admin","Security-Projects"



$Group1 = "User1", "User2"
$Group2 = "User1", "User2"
$Group3 = "User1", "User2"
$Group4 = "User1", "User2"
$Group5 = Get-MsolUser | Where-Object department -like {Group5} | Where-Object islicensed -NotMatch {false}
$Group6 = "User1", "User2"
$Group7 = "User1", "User2"
$Group8 = "User1", "User2"
$Group9 = "User1", "User2"
$Group10 = Get-MsolUser | Where-Object department -like {security} | Where-Object islicensed -NotMatch {false}

$Group1Guid = $Group1 | % {Get-MsolUser -UserPrincipalName $_}
$Group2Guid = $Group2 | % {Get-MsolUser -UserPrincipalName $_}
$Group3Guid = $Group3 | % {Get-MsolUser -UserPrincipalName $_}
$Group4Guid = $Group4 | % {Get-MsolUser -UserPrincipalName $_}
$Group6Guid = $Group6 | % {Get-MsolUser -UserPrincipalName $_}
$Group7Guid = $Group7| % {Get-MsolUser -UserPrincipalName $_}
$Group8Guid = $Group8 | % {Get-MsolUser -UserPrincipalName $_}
$Group9Guid = $Group9 | % {Get-MsolUser -UserPrincipalName $_}



#$groups | %  {New-MsolGroup -DisplayName $_}
<#
$groups[0]  = Get-MsolGroup | Where-Object DisplayName -eq $groups[0]
foreach ($guid in $Group1Guid){
    Add-MsolGroupMember -GroupObjectId $groups[0].objectID.guid -GroupMemberObjectId $Guid.objectID
}
#>
$groups[1]  = Get-MsolGroup | Where-Object DisplayName -eq $groups[1]
foreach ($guid in $Group2Guid){
    Add-MsolGroupMember -GroupObjectId $groups[1].objectID.guid -GroupMemberObjectId $guid.objectID
}
$groups[2]  = Get-MsolGroup | Where-Object DisplayName -eq $groups[2]
foreach ($guid in $Group3Guid){
    Add-MsolGroupMember -GroupObjectId $groups[2].objectID.guid -GroupMemberObjectId $Guid.objectID
}
$groups[3]  = Get-MsolGroup | Where-Object DisplayName -eq $groups[3]
foreach ($guid in $Group4Guid){
    Add-MsolGroupMember -GroupObjectId $groups[3].objectID.guid -GroupMemberObjectId $Guid.objectID
}
$groups[4]  = Get-MsolGroup | Where-Object DisplayName -eq $groups[4]
foreach ($guid in $Group5){
    Add-MsolGroupMember -GroupObjectId $groups[4].objectID.guid -GroupMemberObjectId $guid.objectID
}
$groups[5]  = Get-MsolGroup | Where-Object DisplayName -eq $groups[5]
foreach ($guid in $Group6Guid){
    Add-MsolGroupMember -GroupObjectId $groups[5].objectID.guid -GroupMemberObjectId $Guid.objectID
}
$groups[6]  = Get-MsolGroup | Where-Object DisplayName -eq $groups[6]
foreach ($guid in $Group7Guid){
    Add-MsolGroupMember -GroupObjectId $groups[6].objectID.guid -GroupMemberObjectId $guid.objectid
    Guid.objectID
}
$groups[7]  = Get-MsolGroup | Where-Object DisplayName -eq $groups[7]
foreach ($guid in $Group8Guid){
    Add-MsolGroupMember -GroupObjectId $groups[7].objectID.guid -GroupMemberObjectId $Guid.objectID
}
$groups[8]  = Get-MsolGroup | Where-Object DisplayName -eq $groups[8]
foreach ($guid in $Group9Guid){
    Add-MsolGroupMember -GroupObjectId $groups[8].objectID.guid -GroupMemberObjectId $Guid.objectID
}
$groups[9]  = Get-MsolGroup | Where-Object DisplayName -eq $groups[9]
foreach ($guid in $Group10){
    Add-MsolGroupMember -GroupObjectId $groups[9].objectID.guid -GroupMemberObjectId $guid.objectID
}
