$scripts = get-childitem | where-object name -like *.ps1
Foreach ($script in $script)
  {
    try
    {
      Invoke-Scriptanalyzer -path $script
    }
    catch
    {
      exit 1
    }
}
Foreach ($script in $scripts)
  {
    Invoke-Scriptanalyzer -path $script
  }
