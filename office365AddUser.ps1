﻿#*=============================================================================
#* Script Name: Office365AddUser.ps1
#* Created:     2017-2-7
#* Revised:     2017-5-22
#* Author:  Matthew Dudlo
#* Purpose:     This Script adds users to Office365
#*
#*=============================================================================
#*=============================================================================
#* PARAMETER DECLARATION
#*=============================================================================
param(
    [Paramaeter(Mandatory = $True)][string]$license
)
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* See git Release Notes
#*=============================================================================
#* IMPORT LIBRARIES
#*=============================================================================

#*=============================================================================
#* PARAMETERS
#*=============================================================================

#*=============================================================================
#* INITIALISE VARIABLES
#*=============================================================================
# Increase buffer width/height to avoid PowerShell from wrapping the text in
# output files
$tld = "domain.com"
$domain="@$tld"
$tenantID = "yourtenantID"
#$pshost = get-host
#$pswindow = $pshost.ui.rawui
#$newsize = $pswindow.buffersize
#$newsize.height = 3000
#$newsize.width = 500
#$pswindow.buffersize = $newsize
#*=============================================================================
#* EXCEPTION HANDLER
#*=============================================================================

#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
 #*=============================================================================
#* Function:    get-userinfo
#* Created:     2017-2-7
#* Revised:     2017-5-22
#* Author:  Matthew Dudlo
#* Purpose:     This function takes user input
#* =============================================================================
function get-userinfo{

while ($confirm -notmatch 'yes')
    {
    $script:email = $null
    Write-Host "Enter the user's first name: " -NoNewline
    $script:fn = Read-Host
    Write-Host "Enter the user's last name: " -NoNewline
    $script:ln = Read-Host
    $defaultEmail = $fn[0]+$ln+$domain
    Write-Host "Enter the user's email address [$defaultEmail]: " -NoNewline
    $script:email = Read-Host
    if ($script:email.length -eq 0)
        {
         $script:email = $defaultEmail
         }

    Write-Host "Enter the user's department : " -NoNewline
    $script:dept = Read-Host
    Write-Host "Enter the user's title : " -NoNewline
    $script:title = Read-Host
    $script:fullName = $fn+ ' '+ $ln

    Write-Host -ForegroundColor Yellow 'Confirm'

    write-host "First Name:    " -NoNewline
    $fn
    write-host "Last Name:     " -NoNewline
    $ln
    write-host "email address: " -NoNewline
    $email
    write-host "Department:    " -NoNewline
    $dept
    Write-Host "Title:         " -NoNewline
    $title
    Write-Host -ForegroundColor Green "Type 'yes' to confirm the information is correct:  " -NoNewline
    $confirm = Read-Host
    }
}
#*=============================================================================
#* Function:    Add-NewUser
#* Created:     2017-2-7
#* Revised:     2017-5-22
#* Author:  Matthew Dudlo
#* Purpose:     This function creates the user account
#* =============================================================================
function Add-NewUser {
    Write-Progress -Activity "Creating user $email" -Status "adding account" -PercentComplete 0
    if ($license -match "suite" ) {
        New-MsolUser -DisplayName $fullName -FirstName $fn -LastName $ln -UserPrincipalName $email -UsageLocation "US" -LicenseAssignment $($tenantID):O365_BUSINESS_PREMIUM -OutVariable script:details > $null
    }
    if ($license -match "essentials" ) {
        New-MsolUser -DisplayName $fullName -FirstName $fn -LastName $ln -UserPrincipalName $email -UsageLocation "US" -LicenseAssignment $($tenantID):O365_BUSINESS_ESSENTIALS -OutVariable script:details > $null
    }
    Write-Progress -Activity "Creating user $email" -Status "setting attributes" -PercentComplete 25
    Set-MsolUser -Department $dept -Title $title -UserPrincipalName $email
    Write-Progress -Activity "Creating user $email" -Status "assigning licenses" -PercentComplete 30
    Set-MsolUserLicense -UserPrincipalName $email -AddLicenses $($tenantID):EMS
}
#*=============================================================================
#* Function:    Join-AlluserGroup
#* Created:     2017-2-7
#* Revised:     2017-2-20
#* Author:  Matthew Dudlo
#* Purpose:     This function adds the user to the 'all users' group
#* =============================================================================


function Join-AlluserGroup {
    while ($exist.windowsemailaddress -notmatch $email){
        $exist = get-mailbox -Identity $email -erroraction silentlycontinue
    Write-Progress -Activity "Adding $email to group 'all users' " -Status "Waiting for Cloud Service" -PercentComplete 75
    $counter = 1
    while ($counter -lt '10'){
        Write-Host -ForegroundColor Green "." -NoNewline
        Start-Sleep -Seconds 1
        $counter++
        }

    }
write-host " "

Add-DistributionGroupMember -Identity "all users" -Member $email
}



#*=============================================================================
#* END OF FUNCTION LISTINGS
#*=============================================================================
#*=============================================================================
#* SCRIPT BODY
#*=============================================================================

$UserCredential = Get-Credential
Connect-MsolService -Credential $UserCredential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.protection.outlook.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection
Import-PSSession $Session > $null

get-userinfo

Add-NewUser

#join-AlluserGroup
Write-Progress -Activity "Adding user $email" -Status "done" -Completed
$details2 = Get-MsolUser -UserPrincipalName $email
write-host "email address: "
    $email
write-host "password:      "
    $details | select-object password
write-host "licenses:      "
    $details2.licenses.AccountSkuId
Remove-PSSession $Session
