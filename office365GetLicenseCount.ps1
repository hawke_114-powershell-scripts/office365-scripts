$sku = "VISIOCLIENT","PROJECTCLIENT","ENTERPRISEPACK","O365_BUSINESS_PREMIUM","EMS","MCOMEETADV","O365_BUSINESS_ESSENTIALS","PROJECTPROFESSIONAL"
$licenses = get-msolaccountsku
foreach ($license in $licenses)
  {
    $name = $license.SkuPartNumber
    $count = $license.activeunits
    $consumed = $license.consumedunits
    $remaining = $count - $consumed
    if ($name -match $sku[0])
      {
        $friendlyName = "Visio"
      }
      if ($name -match $sku[1])
      {
        $friendlyName = "Project Pro"
      }
      if ($name -match $sku[2])
      {
        $friendlyName = "Enterprise E3"
      }
      if ($name -match $sku[3])
      {
        $friendlyName = "Business Premium"
      }
      if ($name -match $sku[4])
      {
        $friendlyName = "MDM"
      }
      if ($name -match $sku[5])
      {
        $friendlyName = "PSTN Conference Number"
      }
      if ($name -match $sku[6])
      {
        $friendlyName = "Business Essentials"
      }
      if ($name -match $sku[7])
      {
        $friendlyName = "Project Online Pro"
      }

      write-host "$friendlyName licenses available:"
      write-host $remaining -foregroundcolor "yellow"
  }
